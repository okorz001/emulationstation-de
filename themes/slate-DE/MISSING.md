# EmulationStation Desktop Edition (ES-DE) - Missing slate-DE theme set data

* 64dd : Nintendo 64DD - consolegame.svg, console.svg, game.svg
* ags: Adventure Game Studio - consolegame.svg, console.svg, controller.svg
* amigacd32: Amiga CD32 - consolegame.svg, console.svg, controller.svg, game.svg
* android: Google Android - consolegame.svg, console.svg, controller.svg, game.svg
* apple2gs: Apple IIGS - consolegame.svg, console.svg, controller.svg, game.svg
* astrocade: Bally Astrocade - consolegame.svg, console.svg, controller.svg, game.svg
* atari5200: Atari 5200 - consolegame.svg, console.svg, controller.svg, game.svg
* atari800: Atari 800 - consolegame.svg, console.svg, controller.svg, game.svg
* atarijaguar: Atari Jaguar - consolegame.svg, console.svg, controller.svg, game.svg
* atarijaguarcd: Atari Jaguar CD - consolegame.svg, console.svg, controller.svg, game.svg
* atarist: Atari ST - controller.svg
* atarixe: Atari XE - consolegame.svg, console.svg, controller.svg, game.svg
* atomiswave: Atomiswave - consolegame.svg, console.svg, game.svg
* auto-allgames - customized colors, consolegame.svg, (uses temporary art)
* auto-favorites - customized colors, consolegame.svg, (uses temporary art)
* auto-lastplayed - customized colors, consolegame.svg, (uses temporary art)
* bbcmicro: BBC Micro - consolegame.svg, console.svg, controller.svg, game.svg
* cdimono1: Philips CD-i - consolegame.svg, console.svg, controller.svg, game.svg
* cdtv: Commodore CDTV - consolegame.svg, console.svg, controller.svg, game.svg
* chailove: Löve game engine - consolegame.svg, console.svg, controller.svg
* channelf: Fairchild Channel F - consolegame.svg, console.svg, controller.svg, game.svg
* coco: Tandy Color Computer - consolegame.svg, console.svg, controller.svg, game.svg
* custom-collections - customized colors, consolegame.svg, (uses temporary art)
* daphne: Daphne Arcade Laserdisc Emulator - consolegame.svg, console.svg, controller.svg, game.svg
* desktop: Desktop applications - consolegame.svg, console.svg, controller.svg
* dragon32: Dragon 32 - consolegame.svg, console.svg, controller.svg, game.svg
* epic: Epic Games Store - consolegame.svg, console.svg, controller.svg, game.svg
* gx4000: Amstrad GX4000 - consolegame.svg, console.svg, controller.svg, game.svg
* intellivision: Intellivision - consolegame.svg, console.svg, controller.svg, game.svg
* j2me: Java 2 Micro Edition (J2ME) - consolegame.svg, console.svg, controller.svg, game.svg
* kodi: Kodi home theatre software - consolegame.svg, console.svg, controller.svg
* lutris: Lutris open gaming platform - consolegame.svg, console.svg, controller.svg, game.svg
* macintosh: Apple Macintosh - consolegame.svg, console.svg, controller.svg, game.svg
* mess: MESS (Multi Emulator Super System) - consolegame.svg, console.svg, controller.svg
* moto: Thomson MO/TO series - consolegame.svg, console.svg, controller.svg, game.svg
* msxturbor: MSX Turbo R - consolegame.svg, console.svg, controller.svg, game.svg
* multivision: Othello Multivision - consolegame.svg, console.svg, controller.svg, game.svg
* n3ds: Nintendo 3DS - consolegame.svg, console.svg, controller.svg, game.svg
* naomi: Sega NAOMI - consolegame.svg, console.svg, game.svg
* naomigd: Sega NAOMI GD-ROM - consolegame.svg, console.svg, game.svg
* neogeocd: SNK Neo Geo CD - consolegame.svg, console.svg, controller.svg, game.svg
* neogeocdjp: SNK Neo Geo CD - consolegame.svg, console.svg, controller.svg, game.svg
* openbor: Open Beats of Rage (OpenBOR) game engine - consolegame.svg, console.svg, controller.svg, game.svg
* oric: Tangerine Computer Systems Oric - consolegame.svg, console.svg, controller.svg, game.svg
* palm: Palm OS - consolegame.svg, console.svg, controller.svg, game.svg
* pc88 : NEC PC-8800 series - consolegame.svg, console.svg, game.svg
* pc98 : NEC PC-9800 series - consolegame.svg, console.svg, game.svg
* pcfx: NEC PC-FX - consolegame.svg, console.svg, controller.svg, game.svg
* pokemini: Nintendo Pokémon Mini - consolegame.svg, console.svg, controller.svg, game.svg
* ports: Source ports - consolegame.svg, console.svg, controller.svg, game.svg
* ps2: Sony PlayStation 2 - consolegame.svg, console.svg, controller.svg, game.svg
* ps3: Sony PlayStation 3 - consolegame.svg, console.svg, controller.svg, game.svg
* ps4: Sony PlayStation 4 - consolegame.svg, console.svg, controller.svg, game.svg
* psvita: Sony PlayStation Vita - consolegame.svg, console.svg, controller.svg, game.svg
* samcoupe: SAM Coupé - consolegame.svg, console.svg, game.svg
* satellaview: Nintendo Satellaview - consolegame.svg, console.svg, game.svg
* saturn: Sega Saturn - consolegame.svg, console.svg, controller.svg, game.svg
* saturnjp: Sega Saturn - consolegame.svg, console.svg, controller.svg, game.svg
* solarus: Solarus game engine - consolegame.svg, console.svg
* spectravideo: Spectravideo - consolegame.svg, console.svg, controller.svg, game.svg
* steam: Steam game distribution service - consolegame.svg, console.svg, controller.svg, game.svg
* stratagus: Stratagus game engine - consolegame.svg, console.svg
* sufami: Bandai SuFami Turbo - consolegame.svg, console.svg, game.svg
* switch: Nintendo Switch - consolegame.svg, console.svg, controller.svg, game.svg
* symbian: Symbian - system info (needs improvement), consolegame.svg, console.svg, controller.svg, game.svg
* tanodragon: Tano Dragon - consolegame.svg, console.svg, controller.svg, game.svg
* ti99: Texas Instruments TI-99 - consolegame.svg, console.svg
* tic80: TIC-80 game engine - consolegame.svg, console.svg, controller.svg
* to8: Thomson TO8 - consolegame.svg, console.svg, controller.svg, game.svg
* trs-80: Tandy TRS 80 - consolegame.svg, console.svg, controller.svg, game.svg
* uzebox: Uzebox - consolegame.svg, console.svg, game.svg
* vectrex: Vectrex - consolegame.svg, console.svg (needs simplification as it takes a long time to rasterize)
* videopac: Magnavox Videopac (Odyssey 2) - update all info and graphics to differentiate European version from USA version
* virtualboy: Nintendo Virtual Boy - consolegame.svg, console.svg (needs simplification as it takes a long time to rasterize)
* wiiu: Nintendo Wii U - consolegame.svg, console.svg, controller.svg, game.svg
* x1:  Sharp X1 - consolegame.svg, console.svg, game.svg
* x68000: Sharp X68000 - consolegame.svg, console.svg (image of actual console)
* xbox: Microsoft Xbox - controller.svg
* xbox360: Microsoft Xbox - consolegame.svg, console.svg, game.svg
* zmachine: Infocom Z-machine - consolegame.svg, console.svg
